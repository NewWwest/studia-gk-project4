﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GKLabFourExerciseOne
{
    class PainterWrapper
    {
        private readonly Bitmap _bitmap;
        private readonly Graphics _graphics;
        private readonly int _width;
        private readonly int _height;
        private int _drawingWallNo;
        private float[,] _zBufor;

        public PainterWrapper(Bitmap bitmap, int width, int height)
        {
            _bitmap = bitmap;
            _graphics = Graphics.FromImage(bitmap);
            _width = width;
            _height = height;
            _zBufor = new float[_width, _height];
            for (int i = 0; i < _width; i++)
            {
                for (int j = 0; j < _height; j++)
                {
                    _zBufor[i, j] = float.MinValue;
                }
            }
        }

        private void Clear()
        {
            _zBufor = new float[_width, _height];
            for (int i = 0; i < _width; i++)
            {
                for (int j = 0; j < _height; j++)
                {
                    _zBufor[i, j] = float.MinValue;
                }
            }
            var whitebrush = new SolidBrush(Color.FromKnownColor(KnownColor.White));
            var Floodrect = new Rectangle(0, 0, _width, _height);
            _graphics.FillRectangle(whitebrush, Floodrect);
        }

        public void Draw(Tetrahedron tetrahedron, Matrix4x4 modelMatrix, Matrix4x4 projectionMatrix, Matrix4x4 viewMatrix)
        {
            Clear();
            DrawTriangles(tetrahedron, modelMatrix, projectionMatrix, viewMatrix);
            DrawEdges(tetrahedron, modelMatrix, projectionMatrix, viewMatrix);
        }

        private void DrawEdges(Tetrahedron tetrahedron, Matrix4x4 modelMatrix, Matrix4x4 projectionMatrix, Matrix4x4 viewMatrix)
        {
            var color = Color.Red;
            var brush = new SolidBrush(color);
            var pen = new Pen(brush, 2f);

            foreach (var triangle in tetrahedron.Model)
            {
                Vector4 p1 = VertexShader(modelMatrix, projectionMatrix, viewMatrix, triangle.p1);
                Vector4 p2 = VertexShader(modelMatrix, projectionMatrix, viewMatrix, triangle.p2);
                Vector4 p3 = VertexShader(modelMatrix, projectionMatrix, viewMatrix, triangle.p3);

                if (p1.X / p1.W < -1 || p1.X / p1.W > 1 ||
                        p1.Y / p1.W < -1 || p1.Y / p1.W > 1 ||
                        p2.X / p2.W < -1 || p2.X / p2.W > 1 ||
                        p2.Y / p2.W < -1 || p2.Y / p2.W > 1 ||
                        p3.X / p3.W < -1 || p3.X / p3.W > 1 ||
                        p3.Y / p3.W < -1 || p3.Y / p3.W > 1)
                    continue;

                var p1x = (p1.X / p1.W + 1) * _width / 2;
                var p1y = (p1.Y / p1.W + 1) * _height / 2;
                var p2x = (p2.X / p2.W + 1) * _width / 2;
                var p2y = (p2.Y / p2.W + 1) * _height / 2;
                var p3x = (p3.X / p3.W + 1) * _width / 2;
                var p3y = (p3.Y / p3.W + 1) * _height / 2;

                _graphics.DrawLine(pen,
                        p1x, p1y,
                        p2x, p2y);

                _graphics.DrawLine(pen,
                        p1x, p1y,
                        p3x, p3y);

                _graphics.DrawLine(pen,
                        p3x, p3y,
                        p2x, p2y);
            }
        }

        private void DrawTriangles(Tetrahedron tetrahedron, Matrix4x4 modelMatrix, Matrix4x4 projectionMatrix, Matrix4x4 viewMatrix)
        {
            int i = 0;
            foreach (var triangle in tetrahedron.Model)
            {
                _drawingWallNo = i++;
                Vector4 p1 = VertexShader(modelMatrix, projectionMatrix, viewMatrix, triangle.p1);
                Vector4 p2 = VertexShader(modelMatrix, projectionMatrix, viewMatrix, triangle.p2);
                Vector4 p3 = VertexShader(modelMatrix, projectionMatrix, viewMatrix, triangle.p3);

                if (p1.X / p1.W < -1 || p1.X / p1.W > 1 ||
                        p1.Y / p1.W < -1 || p1.Y / p1.W > 1 ||
                        p2.X / p2.W < -1 || p2.X / p2.W > 1 ||
                        p2.Y / p2.W < -1 || p2.Y / p2.W > 1 ||
                        p3.X / p3.W < -1 || p3.X / p3.W > 1 ||
                        p3.Y / p3.W < -1 || p3.Y / p3.W > 1)
                    continue;

                var p1x = (p1.X / p1.W + 1) * _width / 2;
                var p1y = (p1.Y / p1.W + 1) * _height / 2;
                var p1z = (p1.Z / p1.W + 1);
                var p2x = (p2.X / p2.W + 1) * _width / 2;
                var p2y = (p2.Y / p2.W + 1) * _height / 2;
                var p2z = (p2.Z / p2.W + 1);
                var p3x = (p3.X / p3.W + 1) * _width / 2;
                var p3y = (p3.Y / p3.W + 1) * _height / 2;
                var p3z = (p3.Z / p3.W + 1);
                FillTriangle(p1x, p1y, p1z, p2x, p2y, p2z, p3x, p3y, p3z);
            }
        }

        private static Vector4 VertexShader(Matrix4x4 modelMatrix, Matrix4x4 projectionMatrix, Matrix4x4 viewMatrix, Vector4 location)
        {
            var temp1 = Matrix4x4.Multiply(projectionMatrix, viewMatrix);
            var temp2 = Matrix4x4.Multiply(temp1, modelMatrix);
            return Vector4.Transform(location, temp2);
        }

        /// <summary>
        /// Ref: https://eduinf.waw.pl/inf/utils/002_roz/2008_22.php
        /// </summary>
        public void FillTriangle(float p1x, float p1y, float p1z, float p2x, float p2y, float p2z, float p3x, float p3y, float p3z)
        {
            var buckets = new Dictionary<int, List<HelpEdge>>(3);
            AddEdgeToBuckets(buckets, p1x, p1y, p2x, p2y);
            AddEdgeToBuckets(buckets, p2x, p2y, p3x, p3y);
            AddEdgeToBuckets(buckets, p3x, p3y, p1x, p1y);
            int yMin = Math.Min((int)p1y, Math.Min((int)p2y, (int)p3y));
            int yMax = Math.Max((int)p1y, Math.Max((int)p2y, (int)p3y));

            var activeEdges = new List<HelpEdge>();
            for (int y = yMin; y < yMax; y++)
            {
                for (int i = 0; i < activeEdges.Count; i++)
                {
                    if (activeEdges[i].V2.Y == y)
                    {
                        activeEdges.RemoveAt(i);
                        i--;    //Process next element at the same position
                    }
                }
                bool newEdges = buckets.TryGetValue(y, out var list);
                if (newEdges)
                    activeEdges.AddRange(list);
                activeEdges.OrderBy(he => he.CurrentX);

                bool selector = false;
                int x2 = int.MinValue;
                int x1 = int.MinValue;

                for (int i = 0; i < activeEdges.Count; i++)
                {
                    if (selector)
                    {
                        x2 = (int)Math.Floor(activeEdges[i].CurrentX);
                        if (x1 <= x2)
                        {
                            for (int xi = x1; xi <= x2; xi++)
                                ColorPixel(xi, y, p1x, p1y, p1z, p2x, p2y, p2z, p3x, p3y, p3z);
                        }
                        else
                        {
                            for (int xi = x2; xi <= x1; xi++)
                                ColorPixel(xi, y, p1x, p1y, p1z, p2x, p2y, p2z, p3x, p3y, p3z);
                        }
                    }
                    else
                    {
                        x1 = (int)Math.Ceiling(activeEdges[i].CurrentX);
                    }
                    selector = !selector;
                    activeEdges[i].CurrentX += activeEdges[i].Dx;
                }
            }
        }

        private void ColorPixel(int x, int y, float p1x, float p1y, float p1z, float p2x, float p2y, float p2z, float p3x, float p3y, float p3z)
        {
            var z = GetZ(x, y, p1x, p1y, p1z, p2x, p2y, p2z, p3x, p3y, p3z);
            if (z > _zBufor[x, y])
            {
                var fragmentColor = FragmentShader();
                _zBufor[x, y] = z;
                _bitmap.SetPixel(x, y, fragmentColor);
            }
        }

        private float GetZ(int x, int y, float p1x, float p1y, float p1z, float p2x, float p2y, float p2z, float p3x, float p3y, float p3z)
        {
            var denominator = new Matrix4x4(p1x, p2x, p3x, 0,
                p1y, p2y, p3y, 0,
                1, 1, 1, 0,
                0, 0, 0, 1);

            var alpha1 = new Matrix4x4(x, p2x, p3x, 0,
                y, p2y, p3y, 0,
                1, 1, 1, 0,
                0, 0, 0, 1);

            var beta1 = new Matrix4x4(p1x, x, p3x, 0,
                p1x, y, p3y, 0,
                1, 1, 1, 0,
                0, 0, 0, 1);

            var alpha = alpha1.GetDeterminant();
            var beta = beta1.GetDeterminant();
            var gamma = 1 - alpha - beta;

            return alpha * p1z + beta * p2z + gamma * p3z;
        }

        private Color FragmentShader()
        {
            switch (_drawingWallNo)
            {
                case 0:
                    return Color.Blue;

                case 1:
                    return Color.Green;

                case 2:
                    return Color.Cyan;

                case 3:
                    return Color.Purple;
                default:
                    break;
            }
            return Color.Black;
        }

        private void AddEdgeToBuckets(Dictionary<int, List<HelpEdge>> buckets, float p1x, float p1y, float p2x, float p2y)
        {
            HelpEdge helpEdge = HelpEdge.FromEdge(p1x, p1y, p2x, p2y);

            if (helpEdge.CurrentX < 0)
                return;

            if (buckets.ContainsKey(helpEdge.V1.Y))
                buckets[helpEdge.V1.Y].Add(helpEdge);
            else
                buckets.Add(helpEdge.V1.Y, new List<HelpEdge>() { helpEdge });
        }

        class HelpEdge
        {
            public Point V1;
            public Point V2;
            public float Dx;
            public float CurrentX;

            public static HelpEdge FromEdge(float p1x, float p1y, float p2x, float p2y)
            {
                var higherIsV1 = p1y < p2y;
                var helpEdge = new HelpEdge()
                {
                    V1 = higherIsV1 ? new Point((int)p1x, (int)p1y) : new Point((int)p2x, (int)p2y),
                    V2 = higherIsV1 ? new Point((int)p2x, (int)p2y) : new Point((int)p1x, (int)p1y)
                };
                if (p1y == p2y) //Horizontal
                {
                    helpEdge.Dx = -1;
                    helpEdge.CurrentX = -1;
                }
                else
                {
                    helpEdge.Dx = (float)(helpEdge.V2.X - helpEdge.V1.X) / (float)(helpEdge.V2.Y - helpEdge.V1.Y);
                    helpEdge.CurrentX = helpEdge.V1.X;
                }
                return helpEdge;
            }
        }
    }
}
