﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace GKLabFourExerciseOne
{
    class Tetrahedron
    {
        public List<Triangle> Model;

        public Tetrahedron()
        {
            Model = new List<Triangle>();

            var p0 = new Vector4(0, 2, 2,1);
            var p1 = new Vector4(2, 0, 2, 1);
            var p2 = new Vector4(2, 2, 0, 1);
            var p3 = new Vector4(0, 0, 0, 1);

            var t1 = new Triangle(p3, p1, p0);
            var t2 = new Triangle(p3, p0, p2);
            var t3 = new Triangle(p3, p2, p1);
            var t4 = new Triangle(p0, p1, p2);

            Model.Add(t1);
            Model.Add(t2);
            Model.Add(t3);
            Model.Add(t4);
        }

        public static Tetrahedron NoOffsetTetrahedron()
        {
            var tetra = new Tetrahedron();

            tetra.Model = new List<Triangle>();

            var p0 = new Vector4(0, 0, 0, 1);
            var p1 = new Vector4(1, 0, 0, 1);
            var p2 = new Vector4(0, 1, 0, 1);
            var p3 = new Vector4(1, 1, 1, 1);

            var t1 = new Triangle(p3, p1, p0);
            var t2 = new Triangle(p3, p0, p2);
            var t3 = new Triangle(p3, p2, p1);
            var t4 = new Triangle(p0, p1, p2);

            tetra.Model.Add(t1);
            tetra.Model.Add(t2);
            tetra.Model.Add(t3);
            tetra.Model.Add(t4);

            return tetra;
        }

        public static Tetrahedron TestTetrahedron()
        {
            var tetra = new Tetrahedron();

            tetra.Model = new List<Triangle>();

            var p0 = new Vector4(-0.5f, 0, 0, 1);
            var p1 = new Vector4(0, -0.5f, 0, 1);
            var p2 = new Vector4(0.5f, 0.5f, 0, 1);
            var p3 = new Vector4(0, 0, 3, 1);

            var t1 = new Triangle(p3, p1, p0);
            var t2 = new Triangle(p3, p0, p2);
            var t3 = new Triangle(p3, p2, p1);
            var t4 = new Triangle(p0, p1, p2);

            tetra.Model.Add(t1);
            tetra.Model.Add(t2);
            tetra.Model.Add(t3);
            tetra.Model.Add(t4);

            return tetra;
        }
    }
}
