﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GKLabFourExerciseOne
{
    public partial class Form1 : Form
    {
        private PainterWrapper _painterWrapper;
        private Timer _rotator;
        private float angle;

        Tetrahedron _tetrahedron;
        Matrix4x4 _modelMatrix;
        Matrix4x4 _projectionMatrix;
        Matrix4x4 _viewMatrix;

        public Form1()
        {
            InitializeComponent();

            angle = 0;
            _rotator = new Timer();
            _rotator.Interval = 1000 / 2;
            _rotator.Tick += Rotate;
        }

        private void Rotate(object sender, EventArgs e)
        {
            angle += 0.2f;
            if (angle > Math.PI * 2)
                angle -= (float)Math.PI * 2;
            _modelMatrix = MatrixFactory.ModelMatrix(angle);
            Repaint();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Bitmap bitmap = new Bitmap(Painter.Width, Painter.Width);
            Painter.BackgroundImage = bitmap;
            _painterWrapper = new PainterWrapper(bitmap, Painter.Width, Painter.Width);
            Initialize((float)Math.PI / 2);
            _rotator.Start();
        }


        private void Initialize(float FOV)
        {
            _modelMatrix = MatrixFactory.ModelMatrix();
            
            float a = (float)Painter.Height / (float)Painter.Width;
            float n = 1;
            float f = 10;

            _projectionMatrix = MatrixFactory.ProjectionMatrix(FOV, a, n, f);
            _viewMatrix = MatrixFactory.AdamViewMatrix();
            _tetrahedron = new Tetrahedron();
            Draw(_tetrahedron, _modelMatrix, _projectionMatrix, _viewMatrix);
        }


        private void Draw(Tetrahedron tetrahedron, Matrix4x4 modelMatrix, Matrix4x4 projectionMatrix, Matrix4x4 viewMatrix)
        {
            _painterWrapper.Draw(tetrahedron, modelMatrix, projectionMatrix, viewMatrix);
            Painter.Refresh();
        }

        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            Debug.WriteLine(FOVSlider.Value);
            Initialize((float)Math.PI*FOVSlider.Value/(float)180);
        }

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            Bitmap bitmap = new Bitmap(Painter.Width, Painter.Width);
            Painter.BackgroundImage = bitmap;
            _painterWrapper = new PainterWrapper(bitmap, Painter.Width, Painter.Width);
            trackBar1_ValueChanged(null, null);
        }
        private void Repaint()
        {
            Draw(_tetrahedron, _modelMatrix, _projectionMatrix, _viewMatrix);
            Painter.Refresh();
        }
    }
}
