﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace GKLabFourExerciseOne
{
    class Triangle
    {
        public Vector4 p1 { get; private set; }
        public Vector4 p2 { get; private set; }
        public Vector4 p3 { get; private set; }

        public Triangle(Vector4 P1, Vector4 P2, Vector4 P3)
        {
            p1 = P1;
            p2 = P2;
            p3 = P3;
        }

        public IEnumerable<(Vector4, Vector4)> Edges()
        {
            yield return (p1, p2);
            yield return (p2, p3);
            yield return (p3, p1);
        }

    }
}
