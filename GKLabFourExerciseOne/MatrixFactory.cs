﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace GKLabFourExerciseOne
{
    static class MatrixFactory
    {
        internal static Matrix4x4 ModelMatrix()
        {
            return new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1);
        }

        internal static Matrix4x4 ModelMatrix(double angle)
        {
            return new Matrix4x4(
                (float)Math.Cos(angle), (float)-Math.Sin(angle), 0, 0,
                (float)Math.Sin(angle), (float)Math.Cos(angle), 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1);
        }

        internal static Matrix4x4 ProjectionMatrix(float fOV, float a, float n, float f)
        {
            float e = 1 / (float)Math.Tan(fOV / 2);

            return new Matrix4x4(
                e, 0, 0, 0,
                0, e / a, 0, 0,
                0, 0, (-f - n) / (f - n), (-2 * f * n) / (f - n),
                0, 0, -1, 0);
        }

        internal static Matrix4x4 ViewMatrix()
        {
            return new Matrix4x4(
                -0.64f, 0.76f, 0f, -0.38f,
                -0.51f, -0.42f, 0.74f, -0.16f,
                0.57f, 0.47f, 0.66f, -5.81f,
                0, 0, 0, 1); ;
        }

        internal static Matrix4x4 AdamViewMatrix()
        {
            return new Matrix4x4(
             0, 1, 0, -0.5f,
             0, 0, 1, -0.5f,
             1, 0, 0, -3,
             0, 0, 0, 1);
        }
    }
}
